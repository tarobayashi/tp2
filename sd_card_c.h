/*
 * sd_card_c.h
 *
 *  Created on: 20/03/2014
 *      Author: shon
 */

#ifndef SD_CARD_C_H_
#define SD_CARD_C_H_

//*****************************************************************************
//
// Defines the size of the buffers that hold the path, or temporary data from
// the SD card.  There are two buffers allocated of this size.  The buffer size
// must be large enough to hold the longest expected full path name, including
// the file name, and a trailing null character.
//
//*****************************************************************************
#define PATH_BUF_SIZE           80

//*****************************************************************************
//
// Defines the size of the buffer that holds the command line.
//
//*****************************************************************************
#define CMD_BUF_SIZE            64

extern const char * StringFromFResult(FRESULT iFResult);
extern void SysTickHandler(void);
extern int Cmd_ls(int argc, char *argv[]);
extern int Cmd_cd(int argc, char *argv[]);
extern int Cmd_pwd(int argc, char *argv[]);
extern int Cmd_cat(int argc, char *argv[]);

#endif /* SD_CARD_C_H_ */
