//*****************************************************************************
//
// blinky.c - Simple example to blink the on-board LED.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>

#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/i2c.h"
#include "utils/uartstdio.h"
#include "fatfs/src/ff.h"
#include "fatfs/src/diskio.h"
#include "driverlib/fpu.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "driverlib/systick.h"
#include "grlib/grlib.h"
#include "utils/cmdline.h"
#include "inc/hw_i2c.h"

#include "inc/hw_types.h"
 uint32_t ADCval;
 uint32_t I2Cval;
//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>Blinky (blinky)</h1>
//!
//! A very simple example that blinks the on-board LED using direct register
//! access.
//
//*****************************************************************************


//*****************************************************************************
//
// This function sets up UART0 to be used for a console to display information
// as the example is running.
//
//*****************************************************************************
void
InitConsole(void)
{
    //
    // Enable GPIO port A which is used for UART0 pins.
    // TODO: change this to whichever GPIO port you are using.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    //
    // Configure the pin muxing for UART0 functions on port A0 and A1.
    // This step is not necessary if your part does not support pin muxing.
    // TODO: change this to select the port/pin you are using.
    //
    GPIOPinConfigure(GPIO_PB0_U1RX);
    GPIOPinConfigure(GPIO_PB1_U1TX);

    //
    // Enable UART0 so that we can configure the clock.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    //
    // Use the internal 16MHz oscillator as the UART clock source.
    //
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    //
    // Select the alternate (UART) function for these pins.
    // TODO: change this to select the port/pin you are using.
    //
    GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    //
    // Initialize the UART for console I/O.
    //
    UARTStdioConfig(0, 115200, 16000000);
}

//*****************************************************************************
//*****************************************************************************
//
// Blink the on-board LED.
//
//*****************************************************************************
int
main(void)
{
	// This array is used for storing the data read from the ADC FIFO. It
		    // must be as large as the FIFO for the sequencer in use.  This example
		    // uses sequence 3 which has a FIFO depth of 1.  If another sequence
		    // was used with a deeper FIFO, then the array size must be changed.
		    //
		    uint32_t pui32ADC0Value[1];

		    //
		    // Set the clocking to run at 20 MHz (200 MHz / 10) using the PLL.  When
		    // using the ADC, you must either use the PLL or supply a 16 MHz clock
		    // source.
		    //
		    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
		                   SYSCTL_XTAL_16MHZ);

		    // Set up the serial console to use for displaying messages.  This is
		    // just for this example program and is not needed for ADC operation.
		    //

		    InitConsole();

		    // Display the setup on the console.
		    //
		    UARTprintf("  ADC ->\n");
		    UARTprintf("  Type: Single Ended\n");
		    UARTprintf("  Samples: One\n");
		    UARTprintf("  Update Rate: 250ms\n");
		    UARTprintf("  Input Pin: AIN0/PE7\n\n");

		    //
		    // The ADC0 peripheral must be enabled for use.
		    //
		    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

		    //
		    // For this example ADC0 is used with AIN0 on port E7.
		    // The actual port and pins used may be different on your part, consult
		    // the data sheet for more information.  GPIO port E needs to be enabled
		    // so these pins can be used.
		    // TODO: change this to whichever GPIO port you are using.
		    //
		    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

		    //
		    // Select the analog ADC function for these pins.
		    // Consult the data sheet to see which functions are allocated per pin.
		    // TODO: change this to select the port/pin you are using.
		    //
		    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);

		    //
		    // Enable sample sequence 3 with a processor signal trigger.  Sequence 3
		    // will do a single sample when the processor sends a signal to start the
		    // conversion.  Each ADC module has 4 programmable sequences, sequence 0
		    // to sequence 3.  This example is arbitrarily using sequence 3.
		    //
		    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);

		    //
		    // Configure step 0 on sequence 3.  Sample channel 0 (ADC_CTL_CH0) in
		    // single-ended mode (default) and configure the interrupt flag
		    // (ADC_CTL_IE) to be set when the sample is done.  Tell the ADC logic
		    // that this is the last conversion on sequence 3 (ADC_CTL_END).  Sequence
		    // 3 has only one programmable step.  Sequence 1 and 2 have 4 steps, and
		    // sequence 0 has 8 programmable steps.  Since we are only doing a single
		    // conversion using sequence 3 we will only configure step 0.  For more
		    // information on the ADC sequences and steps, reference the datasheet.
		    //
		    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH0 | ADC_CTL_IE |
		                             ADC_CTL_END);

		    //
		    // Since sample sequence 3 is now configured, it must be enabled.
		    //
		    ADCSequenceEnable(ADC0_BASE, 3);

		    //
		    // Clear the interrupt status flag.  This is done to make sure the
		    // interrupt flag is cleared before we sample.
		    //
		    ADCIntClear(ADC0_BASE, 3);

		    //
		    // Sample AIN0 forever.  Display the value on the console.
		    //
		    while(1)
		    {
		        //
		        // Trigger the ADC conversion.
		        //
		    	ADCval = 26;
		        ADCProcessorTrigger(ADC0_BASE, 3);

		        //
		        // Wait for conversion to be completed.
		        //
		        while(!ADCIntStatus(ADC0_BASE, 3, false))
		        {
		        }

		        //
		        // Clear the ADC interrupt flag.
		        //
		        ADCIntClear(ADC0_BASE, 3);

		        //
		        // Read ADC Value.
		        //
		        ADCSequenceDataGet(ADC0_BASE, 3, pui32ADC0Value);
		        ADCval = pui32ADC0Value[0];

		        //
		        // Display the AIN0 (PE7) digital value on the console.
		        //
		        UARTprintf("AIN0 = %4d\r", pui32ADC0Value[0]);
		        //
		        // Wait for interrupt to occur.
		        //

		        // This function provides a means of generating a constant length
		        // delay.  The function delay (in cycles) = 3 * parameter.  Delay
		        // 250ms arbitrarily.
		        //
		        SysCtlDelay(SysCtlClockGet() / 12);
		    }
}
